<?php

/**
 * 361GRAD Element Teaser Simple
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_LANG']['CTE']['dse_elements']     = 'DSE-Elemente';
$GLOBALS['TL_LANG']['CTE']['dse_teasertags'] = ['Tagliste', 'Teaser Block.'];

$GLOBALS['TL_LANG']['tl_content']['teasertags_legend']   = 'Tags-Einstellungen';
$GLOBALS['TL_LANG']['tl_content']['dse_teaser_tags']      = ['Tags', ''];
$GLOBALS['TL_LANG']['tl_content']['tt_field_1']      = ['Link Text der linken Spalte', 'Der Linktext wird anstelle der Ziel-URL angezeigt.'];
$GLOBALS['TL_LANG']['tl_content']['tt_field_2']      = ['Link der linken Spalte', 'Bitte geben Sie eine Webadresse (http: // ...), eine E-Mail-Adresse (mailto: ...) oder eine Einfügung ein'];
$GLOBALS['TL_LANG']['tl_content']['tt_field_3']      = ['Linkziel', 'Öffnet den Link in einem neuen Browserfenster.'];