<?php

/**
 * 361GRAD Taglist Element
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

// Element palettes
$GLOBALS['TL_DCA']['tl_content']['palettes']['dse_taglist'] =
    '{type_legend},type,headline;' .
    '{teasertags_legend},dse_teaser_tags;' .
    '{margin_legend},dse_marginTop,dse_marginBottom;' .
    '{invisible_legend:hide},invisible,start,stop';

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_teaser_tags'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_teaser_tags'],
    'exclude'   => true,
    'inputType' => 'multiColumnWizard',
    'eval'      => [
        'tl_class'     => 'clr',
        'columnFields' => [
            'tt_field_1' => [
                'label'     => &$GLOBALS['TL_LANG']['tl_content']['tt_field_1'],
                'inputType' => 'text',
                'eval'      => [
                    'style' => 'width: 40%'
                ],
                'sql'       => "varchar(255) NOT NULL default ''"
            ],
            'tt_field_2' => [
                'label'     => &$GLOBALS['TL_LANG']['tl_content']['tt_field_2'],
                'inputType' => 'text',
                'eval'      => [
                    'style' => 'width: 40%'
                ],
                'wizard'    => [
                    [
                        'tl_content',
                        'pagePicker',
                    ]
                ],
                'sql'       => "varchar(255) NOT NULL default ''"
            ],
            'tt_field_3' => [
                'label'     => &$GLOBALS['TL_LANG']['tl_content']['tt_field_3'],
                'inputType' => 'checkbox',
                'eval'      => [
                    'mandatory' => false,
                    'style' => 'width: 33.33%'
                ],
                'sql'       => "varchar(255) NOT NULL default ''"
            ],
        ]
    ],
    'sql'       => "blob NULL"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_marginTop']    = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_marginTop'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "text NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_marginBottom'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_marginBottom'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "text NULL"
];
